<?php

/**
 * @file
 * This is the file description for Module Mutistep Form Creator.
 *
 * @author vitaliy
 */

module_load_include('inc', 'multistep_form_creator', 'includes/multistep_form_creator.navigation');
module_load_include('inc', 'multistep_form_creator', 'includes/multistep_form_creator.validate');
module_load_include('inc', 'multistep_form_creator', 'includes/multistep_form_creator.submit');
module_load_include('inc', 'multistep_form_creator', 'includes/multistep_form_creator.confirm');

/**
 * Implements hook_install().
 */
function multistep_form_creator_install() {

  $types_form_fields = [
    0 => t('checkbox'),
    1 => t('radios'),
    2 => t('textfield'),
    3 => t('link'),
    4 => t('textarea'),
    5 => t('password'),
    6 => t('password_confirm'),
    7 => t('select'),
    8 => t('date'),
    9 => t('file'),
  ];
  // Default values.
  variable_set('types_fields', $types_form_fields);
}

/**
 * Implements hook_menu().
 */
function multistep_form_creator_menu() {

  $items                           = [];
  $items['multistep-form-creator'] = [
    'title'            => 'Multistep Form',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => ['multistep_form_creator_form'],
    'access arguments' => ['access content'],
    'file'             => 'includes/multistep_form_creator.form.inc',
    'type'             => MENU_NORMAL_ITEM,
  ];

  $items['complete-page'] = [
    'title'            => 'Form Complete',
    'page callback'    => 'multistep_form_creator_complete',
    'page arguments'   => [],
    'access arguments' => ['access content'],
    'type'             => MENU_NORMAL_ITEM,
  ];

  $items['admin/config/module-form-creator'] = [
    'title'            => 'Multistep Form Creator Settings',
    'position'         => 'left',
    'weight'           => -100,
    'page callback'    => 'system_admin_menu_block_page',
    'access arguments' => ['administer site configuration'],
    'file'             => 'system.admin.inc',
    'file path'        => drupal_get_path('module', 'system'),
  ];

  $items['admin/config/module-form-creator/config-form'] = [
    'title'            => 'Module setting',
    'description'      => 'Module settings link',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => ['multistep_form_creator_config_form'],
    'access callback'  => TRUE,
    'access arguments' => ['administer site configuration'],
    'file'             => 'includes/multistep_form_creator.admin.inc',
  ];

  $items['admin/config/module-form-creator/steps']      = [
    'title'            => 'Steps setting',
    'description'      => 'Steps settings',
    'page callback'    => 'steps_list',
    'access callback'  => TRUE,
    'access arguments' => ['administer site configuration'],
    'file'             => 'includes/multistep_form_creator.admin.steps.inc',
  ];
  $items['admin/config/module-form-creator/steps/list'] = [
    'title'       => 'Steps setting list',
    'description' => 'Steps settings list',
    'type'        => MENU_DEFAULT_LOCAL_TASK,
    'weight'      => 1,
  ];

  $sum_steps = variable_get('admin_steps_amount', 3);

  for ($step = 1; $step <= $sum_steps; $step++) {
    $items['admin/config/module-form-creator/steps/' . $step] = [
      'title'            => 'Step ' . $step . ' setting',
      'description'      => 'Config step',
      'page callback'    => 'drupal_get_form',
      'page arguments'   => ['multistep_form_creator_config_step_form', 4],
      'access arguments' => ['administer site configuration'],
      'type'             => MENU_CALLBACK,
      'file'             => 'includes/multistep_form_creator.admin.inc',
    ];
  }

  return $items;
}

/*
 * Implements hook_mail().
 */
function multistep_form_creator_mail($key, &$message, $params) {

  if (isset($params['subject'])) {
    $message['subject'] = $params['subject'];
  }
  if (isset($params['body'])) {
    $message['body'][] = $params['body'];
  }
  if (isset($params['headers']) && is_array($params['headers'])) {
    $message['headers'] += $params['headers'];
  }

}

/**
 * Implements hook_action_info().
 */
function multistep_form_creator_theme() {
  $path = drupal_get_path('module', 'multistep_form_creator');
  $base = [
    'path' => "$path/views/themes",
  ];

  return [
    'confirm_details'            => $base + [
        'variables' => [
          'details' => NULL,
        ],
        'template'  => 'confirm-details',
      ],
    'confirm_details_expandable' => $base + [
        'variables' => [
          'details' => NULL,
        ],
        'template'  => 'confirm-details-expandable',
      ],
  ];
}

/**
 * Returns values for select type_field.
 *
 * @param string|bool $index
 *   Key prefix.
 *
 * @return array
 *   Options.
 */
function get_select_values($index = FALSE) {
  if (FALSE === $index) {
    return [];
  }
  $options = [];
  $strings = (FALSE !== explode("\r\n", variable_get($index . '_select_choices'))) ? explode("\r\n", variable_get($index . '_select_choices')) : [];
  foreach ($strings as $string) {
    $key                    = explode(':', $string);
    $options[trim($key[0])] = trim($key[1]);
  }
  return $options;
}

/**
 * Returns data for the confirmation step.
 *
 * @param string|int $type_field
 *   Option param.
 * @param string|int $data
 *   Form state value.
 *
 * @return int|null|string
 */
function get_confirm_data($type_field, $data = 0) {
  switch (intval($type_field)) {
    // If checkbox.
    case 0:
      if (intval($data) == 0) {
        $data = 'No Checked';
      }
      else {
        $data = 'Checked';
      }
      break;

    // If textfield, textarea.
    case 2:
    case 4:
      // If password.
    case 6:
      // If select.
    case 7:
      if (empty($data)) {
        $data = t('---');
      }
      break;

    // If date.
    case 8:
      if (empty($data)) {
        $data = t('---');
      }
      $result = '';
      foreach ($data as $key => $value) {
        $result .= $value . ' / ';
      }
      $data = trim($result, ' / ');
      break;

    // If file.
    case 9:
      if (empty($data)) {
        $data = 'No file';
      }
      break;
  }
  return $data;
}

/**
 * Returns key for the form_state values.
 *
 * @param int $number1
 *   Loops data.
 * @param int $number2
 *   Loops data.
 *
 * @return string
 *   Key for $form_state values.
 */
function get_prefix($number1 = 0, $number2 = 0) {
  $key = '';
  $key .= empty($number1) ? 's' : 's' . trim($number1);
  $key .= empty($number2) ? '_fs' : '_fs' . trim($number2);
  return $key;
}

/**
 * Returns key for the form_state values.
 *
 * @param int $number
 *   Loops data.
 *
 * @return string
 *   Key for $form_state values.
 */
function get_fieldset_num($number = 0) {
  $key = '';
  $key .= empty($number) ? 'fieldset' : 'fieldset_' . trim($number);
  return $key;
}

/**
 * Returns key for the form_state values.
 *
 * @param string $prefix
 *   Prefix.
 * @param int $number
 *   Loops data.
 *
 * @return string
 *   Key for $form_state values.
 */
function get_index($prefix = '', $number = 0) {
  $key = '';
  $key .= empty($prefix) ? '' : trim($prefix);
  $key .= empty($number) ? '_f' : '_f' . trim($number);
  return $key;
}

/**
 * Returns aviable file extensions.
 *
 * @param string $key
 *   File field key.
 *
 * @return array
 *   Aviable extensions.
 */
function get_file_extensions($key = '') {
  $result = [];

  if (empty($key)) {
    return $result;
  }

  $string = '';
  $ext_key = rtrim($key, 'title_field_file');
  $extensions = variable_get($ext_key . '_file_extensions') ? explode("\n", variable_get($ext_key . '_file_extensions')) : [];
  foreach ($extensions as $extension) {
    $string .= trim($extension) . ' ';
  }
  $result[] = $string;
  return $result;
}
