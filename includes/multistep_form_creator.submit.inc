<?php

/**
 * @file
 * Handles the form submission of the multistep form creator form.
 */

/**
 * Handles what to do with the submitted form.
 *
 * Depending on what stage has been completed.
 *
 * @see multistep_form_creator_form()
 * @see multistep_form_creator_form_validate()
 */
function multistep_form_creator_form_submit($form, &$form_state) {
  $steps   = variable_get('admin_steps_amount', 3);
  $confirm = $steps + 1;
  $finish  = $steps + 2;

  switch ($form_state['stage']) {

    case $confirm:
      $form_state['multistep_values'][$form_state['stage']] = $form_state['values'];

      for ($s = 1; $s <= $steps; $s++) {
        if ($form_state['triggering_element']['#value'] == t('Edit @form', ['@form' => variable_get('s' . $s . '_title')])) {
          $form_state['new_stage'] = $s;
        }
      }

      if ($form_state['triggering_element']['#value'] == 'Next') {
        multistep_form_creator_confirm_submit($form, $form_state);
        $form_state['complete'] = TRUE;
      }
      break;

    case $finish:
      $form_state['multistep_values'][$form_state['stage']] = $form_state['values'];
      if ($form_state['triggering_element']['#value'] != 'Back') {
        multistep_form_creator_confirm_submit($form, $form_state);
        $form_state['complete'] = TRUE;
      }
      break;

    default:
      if (TRUE === $form_state['has_file_element']) {
        mfc_submit_file($form, $form_state);
      }
      $form_state['multistep_values'][$form_state['stage']] = $form_state['values'];
      $form_state['new_stage']                              = multistep_form_creator_move_to_next_stage($form, $form_state);
      break;

  }

  if (isset($form_state['complete'])) {
    drupal_goto('complete-page');
  }

  if ('Back' === $form_state['triggering_element']['#value']) {
    $form_state['new_stage'] = multistep_form_creator_move_to_previous_stage($form, $form_state);
  }

  if (isset($form_state['multistep_values']['form_build_id'])) {
    $form_state['values']['form_build_id'] = $form_state['multistep_values']['form_build_id'];
  }

  $form_state['multistep_values']['form_build_id'] = $form_state['values']['form_build_id'];
  $form_state['stage']                             = $form_state['new_stage'];
  $form_state['rebuild']                           = TRUE;
}

/**
 * Handles the submission of the final stage.
 *
 * Sends an email to the user confirming their entry.
 */
function multistep_form_creator_confirm_submit($form, &$form_state) {

  $multistep_values = $form_state['multistep_values'][$form_state['stage']];

  $module = 'multistep_form_creator';
  $key    = 'multistep_form_creator_complete';
  $to     = $multistep_values['confirm']['term']['email'] . '; ' . variable_get('site_mail', 'admin@example.com');
  $from   = variable_get('site_mail', 'admin@example.com');
  $params = [
    'body'    => variable_get('message', ''),
    'subject' => 'Thank you for filling in our survey, ' . $multistep_values['confirm']['term']['email'],
  ];

  $language = language_default();
  $send     = TRUE;
  $result   = drupal_mail($module, $key, $to, $language, $params, $from, $send);
  if (TRUE == $result['result']) {
    drupal_set_message(t('Your message has been sent.'));
  }
  else {
    drupal_set_message(t('There was a problem sending your message and it was not sent. @name', ['@name' => $multistep_values['confirm']['term']['email']]), 'error');
  }

}

/**
 * File submitter.
 *
 * @param $form_state
 */
function mfc_submit_file(&$form, $form_state) {
  foreach ($form_state['values'] as $value => $name) {
    if (!isset($form_state['storage'][$value])) {
      continue;
    }
    // Delete previously uploaded file.
    if (variable_get($form_state['stage'] . $value)) {
      $fid = variable_get($form_state['stage'] . $value);
      $file = file_load($fid);
      file_delete($file);
    }
    $file = $form_state['storage'][$value];
    variable_set($form_state['stage'] . $value, $file->fid);
    unset($form_state['storage'][$value]);
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);
    drupal_set_message(t('The form has been submitted and the image has been saved, filename: @filename.', ['@filename' => $file->filename]));
  }
}

/**
 * Returns what to show on the completion page.
 */
function multistep_form_creator_complete() {

  return t('Thank you for completing our survey. You have been sent an email confirming you\'ve been entered into our prize draw');

}
