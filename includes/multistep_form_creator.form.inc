<?php

/**
 * @file
 * Handles the form elements of the multistep form creator.
 *
 * All hooks are in the .module file.
 */

/**
 * Master form which calls an individual form for each step.
 *
 * @see multistep_form_creator_form_validate()
 * @see multistep_form_creator_form_submit()
 */
function multistep_form_creator_form($form, &$form_state) {

  drupal_add_css(drupal_get_path('module', 'multistep_form_creator') . '/css/multistep_form_creator.css');

  if (!isset($form_state['stage'])) {
    $form_state['stage'] = 1;
  }
  $steps         = variable_get('admin_steps_amount', 3);
  $confirm       = intval($steps) + 1;
  $finish        = intval($steps) + 2;
  $is_expandable = variable_get('s' . $form_state['stage'] . '_expandable_step');

  $form = [];
  $form = multistep_form_creator_get_header($form, $form_state);

  $values = isset($form_state['multistep_values'][$form_state['stage']]) ? $form_state['multistep_values'][$form_state['stage']] : [];
  // If 'confirm' stage.
  if ($form_state['stage'] == $confirm) {
    return multistep_form_creator_confirm_form($form, $form_state);
  }

  $form[$form_state['stage']] = [
    '#type'        => 'fieldset',
    '#title'       => variable_get('s' . $form_state['stage'] . '_active_step_title') ? variable_get('s' . $form_state['stage'] . '_step_title') : '',
    '#description' => variable_get('s' . $form_state['stage'] . '_active_step_description') ? variable_get('s' . $form_state['stage'] . '_step_description') : '',
  ];

  // If step is expandable.
  if (1 === $is_expandable) {
    $form['#tree'] = TRUE;

    $form[$form_state['stage']]['#prefix'] = '<div id="replace-this">';
    $form[$form_state['stage']]['#suffix'] = '</div>';

    if (!array_key_exists('clicked_button', $form_state)) {
      multistep_form_creator_expandable_form($form, $form_state, 0, $values);
    }
    else {
      if (isset($values[$form_state['stage']])) {
        foreach ($values[$form_state['stage']] as $rows => $value) {
          multistep_form_creator_expandable_form($form, $form_state, $rows, $values);
        }
      }

      $last = isset($form_state['values'][$form_state['stage']]) ? count($form_state['values'][$form_state['stage']]) : 0;
      multistep_form_creator_expandable_form($form, $form_state, $last, $values);
    }

  }
  // If step is not expandable.
  else {
    multistep_form_creator_simple_form($form, $form_state, $values);

  }

  if (1 !== $form_state['stage']) {
    $form['back'] = [
      '#type'  => 'submit',
      '#value' => t('Back'),
    ];
  }

  $form['next'] = [
    '#type'  => 'submit',
    '#value' => t('Next'),
  ];

  return $form;

}

/**
 * Data for the not expandable type of the forms.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 * @param array $values
 *   Multistep values.
 */
function multistep_form_creator_simple_form(array &$form, array $form_state, array $values = []) {
  $types_fields = variable_get('types_fields');

  // Start fisldset.
  for ($k = 1; $k <= 9; $k++) {
    // For always displaying first fieldset in active step.
    $prefix          = get_prefix($form_state['stage'], $k);
    $fieldset_num    = get_fieldset_num($k);
    $length_fieldset = variable_get($prefix . '_f') ? variable_get($prefix . '_f') : 0;

    // If first field in first fieldset and no settings.
    if (!empty($length_fieldset)) {
      $form[$form_state['stage']][$fieldset_num] = [
        '#type'        => 'fieldset',
        '#title'       => variable_get($prefix . '_active_title_fieldset') ? variable_get($prefix . '_title_fieldset') : '',
        '#description' => variable_get($prefix . '_active_description_fieldset') ? variable_get($prefix . '_description_fieldset') : '',
      ];

    }
    else {
      continue;
    }

    // Start fields.
    for ($y = 1; $y <= $length_fieldset; $y++) {
      $index = get_index($prefix, $y);

      if (variable_get($index . '_active_field') !== NULL || $y === 1) {

        $type_field = variable_get($index . '_type_field');

        $form[$form_state['stage']][$fieldset_num][$index . '_title_field'] = [
          '#type'          => $types_fields[$type_field],
          '#title'         => variable_get($index . '_show_title_field') ? check_plain(variable_get($index . '_title_field')) : '',
          '#description'   => variable_get($index . '_show_description_field') ? check_plain(variable_get($index . '_description_field')) : '',
          '#default_value' => isset($values[$index . '_title_field']) ? $values[$index . '_title_field'] : '',
          '#required'      => variable_get($index . '_required', FALSE),
        ];
        switch (intval($type_field)) {
          // If type_field is radio.
          case 1:
            $options = [
              0 => variable_get($index . '_radios_choice_1'),
              1 => variable_get($index . '_radios_choice_2'),
            ];

            $form[$form_state['stage']][$fieldset_num][$index . '_title_field']['#options'] = $options;
            break;

          // If type_field is password.
          case 5:
          case 6:
            unset($form[$form_state['stage']][$fieldset_num][$index . '_title_field']['#default_value']);
            $form[$form_state['stage']][$fieldset_num][$index . '_title_field']['#size'] = 15;
            break;

          // If type_field is select.
          case 7:
            $options                                                                        = get_select_values($index);
            $form[$form_state['stage']][$fieldset_num][$index . '_title_field']['#options'] = $options;
            break;

          // If type_field is date.
          case 8:
            break;

          // If type_field is file.
          case 9:
            unset($form[$form_state['stage']][$fieldset_num][$index . '_title_field']);
            $form[$form_state['stage']][$fieldset_num][$index . '_title_field_file'] = [
              '#type'        => $types_fields[$type_field],
              '#title'       => variable_get($index . '_show_title_field') ? check_plain(variable_get($index . '_title_field')) : '',
              '#description' => variable_get($index . '_show_description_field') ? check_plain(variable_get($index . '_description_field')) : '',
              '#default_value' => variable_get($form_state['stage'] . $index . '_title_field_file') ? variable_get($form_state['stage'] . $index . '_title_field_file') : '',
              '#required'    => variable_get($index . '_required', FALSE),
            ];

            $fid  = variable_get($form_state['stage'] . $index . '_title_field_file') ? variable_get($form_state['stage'] . $index . '_title_field_file') : '';
            $file = file_load($fid);
            if (!empty($file)) {
              $url                                                           = parse_url(file_create_url($file->uri));
              $form[$form_state['stage']][$fieldset_num][$index . '_murkup'] = [
                '#prefix' => '<div>',
                '#suffix' => '</div>',
                '#markup' => '<img src="' . $url['path'] . '" width="100" height="100">',
              ];
            }
            break;
        }
      }
    }
  }
}

/**
 * Data for the expandable type of the forms.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Current form state.
 * @param int $rows
 *   Subform's number.
 * @param array $values
 *   Multistep values.
 */
function multistep_form_creator_expandable_form(array &$form, array $form_state, $rows = 0, array $values = []) {
  $types_fields = variable_get('types_fields');

  if (empty($form_state[$form_state['stage']]['num_sub_form'])) {
    $form_state[$form_state['stage']]['num_sub_form'] = 0;
  }

  // Start additional subform.
  for ($row = 0; $row <= $rows; $row++) {
    // Start fieldset.
    for ($fs = 1; $fs <= 9; $fs++) {
      $prefix          = get_prefix($form_state['stage'], $fs);
      $fieldset_num    = get_fieldset_num($fs);
      $length_fieldset = variable_get($prefix . '_f') ? variable_get($prefix . '_f') : 0;

      // If first field in first fieldset and no settings.
      if (!empty($length_fieldset)) {
        $form[$form_state['stage']][$row][$fieldset_num] = [
          '#type'        => 'fieldset',
          '#title'       => variable_get($prefix . '_active_title_fieldset') ? variable_get($prefix . '_title_fieldset') : '',
          '#description' => variable_get($prefix . '_active_description_fieldset') ? variable_get($prefix . '_description_fieldset') : '',
        ];
      }
      else {
        continue;
      }

      // Start field.
      for ($f = 1; $f <= $length_fieldset; $f++) {

        $index = get_index($prefix, $f);

        if (NULL !== variable_get($index . '_active_field') || 1 === $f) {

          $type_field = variable_get($index . '_type_field');

          $form[$form_state['stage']][$row][$fieldset_num][$index . '_title_field'] = [
            '#type'          => $types_fields[$type_field],
            '#title'         => variable_get($index . '_show_title_field') ? check_plain(variable_get($index . '_title_field')) : '',
            '#description'   => variable_get($index . '_show_description_field') ? check_plain(variable_get($index . '_description_field')) : '',
            '#default_value' => isset($values[$form_state['stage']][$row][$fieldset_num][$index . '_title_field']) ?
              $values[$form_state['stage']][$row][$fieldset_num][$index . '_title_field'] : '',
            '#required'      => variable_get($index . '_required', FALSE),
          ];
          switch (intval($type_field)) {
            // If type_field is radio.
            case 1:
              $options                                                                              = [
                0 => variable_get($index . '_radios_choice_1'),
                1 => variable_get($index . '_radios_choice_2'),
              ];
              $form[$form_state['stage']][$row][$fieldset_num][$index . '_title_field']['#options'] = $options;
              break;

            // If type_field is password.
            case 5:
            case 6:
              unset($form[$form_state['stage']][$row][$fieldset_num][$index . '_title_field']['#default_value']);
              $form[$form_state['stage']][$row][$fieldset_num][$index . '_title_field']['#size'] = 15;
              break;

            // If type_field is select.
            case 7:
              $options                                                                              = get_select_values($index);
              $form[$form_state['stage']][$row][$fieldset_num][$index . '_title_field']['#options'] = $options;
              break;

            // If type_field is date.
            case 8:
              break;

            // If type_field is file.
            case 9:
              unset($form[$form_state['stage']][$row][$fieldset_num][$index . '_title_field']);
              $form[$form_state['stage']][$row][$fieldset_num][$index . '_title_field_file'] = [
                '#type'          => $types_fields[$type_field],
                '#title'         => variable_get($index . '_show_title_field') ? check_plain(variable_get($index . '_title_field')) : '',
                '#description'   => variable_get($index . '_show_description_field') ? check_plain(variable_get($index . '_description_field')) : '',
                '#default_value' => isset($values[$form_state['stage']][$row][$fieldset_num][$index . '_title_field']) ?
                  $values[$form_state['stage']][$row][$fieldset_num][$index . '_title_field'] : '',
                '#required'      => variable_get($index . '_required', FALSE),
              ];
              break;
          }
        }
      }
    }
  }

  $form['another'] = [
    '#type'     => 'submit',
    '#value'    => t('Add more'),
    '#validate' => ['multistep_form_creator_expandable_form_another'],
    '#ajax'     => [
      'callback' => 'ajax_simplest_callback',
      'wrapper'  => 'replace-this',
      'method'   => 'replace',
      'effect'   => 'fade',
    ],
  ];

}

/**
 * Additional form of the contacts_name stage.
 */
function multistep_form_creator_expandable_form_another(array $form, array &$form_state) {
  $form_state[$form_state['stage']]['num_sub_form']++;
  $form_state['storage']['another'] = TRUE;
  $form_state['rebuild']            = TRUE;
}

/**
 * Ajax handler of the contacts_name step.
 */
function ajax_simplest_callback(array $form, array $form_state) {
  return $form[$form_state['stage']];
}

/**
 * Form for the confirm step.
 *
 * @see multistep_form_creator_form()
 */
function multistep_form_creator_confirm_form(array $form, array &$form_state) {

  $values = isset($form_state['multistep_values'][$form_state['stage']]) ? $form_state['multistep_values'][$form_state['stage']] : [];

  $form['confirm'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Confirm Details'),
    '#description' => t('Please checkall of your details carefully before confirming your winery submission. Enter details foreach wine you will be submitting'),
    '#tree'        => TRUE,
  ];

  $steps = variable_get('admin_steps_amount', 3);

  // Start the information about completed steps.
  for ($step = 1; $step <= $steps; $step++) {
    $form['confirm'][$step] = [
      '#type'  => 'fieldset',
      '#title' => variable_get('s' . $step . '_title', 'Step-' . $step),
      '#tree'  => TRUE,
    ];
    // If is expandable step.
    if (isset($form_state[$step]['num_sub_form']) || isset($form_state['multistep_values'][$step]['another'])) {
      $form['confirm'][$step]['details'] = [
        '#markup' => multistep_form_creator_confirm_details_expandable_form($form, $form_state, $step),
      ];
    }
    else {
      $form['confirm'][$step]['details'] = [
        '#markup' => multistep_form_creator_confirm_details_form($form, $form_state, $step),
      ];
    }
    $form['confirm'][$step]['edit'] = [
      '#type'  => 'submit',
      '#value' => t('Edit @form', ['@form' => variable_get('s' . $step . '_title', 'Step-' . $step)]),
    ];
  }

  // Form confirm.
  $form['confirm']['term']                 = [
    '#type'  => 'fieldset',
    '#title' => t('Terms'),
    '#tree'  => TRUE,
  ];
  $form['confirm']['term']['term_details'] = [
    '#type'          => 'checkbox',
    '#title'         => t("I/we accept the 'Submission Terms'"),
    '#default_value' => isset($values['term']['term_details']) ? $values['term']['term_details'] : NULL,
    '#ajax'          => [
      'callback' => 'ajax_term_callback',
      'wrapper'  => 'multistep-form-creator-form',
      'method'   => 'replace',
      'effect'   => 'fade',
    ],
  ];
  $form['confirm']['term']['email']        = [
    '#type'          => 'textfield',
    '#title'         => t("Email"),
    '#description'   => t('Enter email address'),
    '#default_value' => isset($values['term']['email']) ? $values['term']['email'] : NULL,
  ];

  $form['back'] = [
    '#type'  => 'submit',
    '#value' => t('Back'),
  ];

  $form['next'] = [
    '#type'   => 'submit',
    '#value'  => t('Next'),
    '#states' => [
      'disabled' => [
        ':input[name="confirm[term][term_details]"]' => ['checked' => FALSE],
      ],
      'enabled'  => [
        ':input[name="confirm[term][term_details]"]' => ['checked' => TRUE],
      ],
    ],
  ];
  return $form;
}

/**
 * Ajax handler.
 */
function ajax_term_callback(array $form, array &$form_state) {

  $form_state['rebuild'] = TRUE;
  return $form;
}

/**
 * Form for the finished step.
 *
 * @see multistep_form_creator_form()
 */
function multistep_form_creator_finished_form(array $form, array &$form_state) {

  multistep_form_creator_confirm_submit($form, $form_state);
  drupal_set_message(multistep_form_creator_complete());

  $form['return'] = [
    '#type'  => 'submit',
    '#value' => t('Return to Home Page'),
  ];

  return $form;
}
