<?php

/**
 * @file
 * Form cunfiguration of the multistep form creator module.
 */

module_load_include('inc', 'multistep_form_creator', 'includes/multistep_form_creator.admin.submit');

/**
 * MultiStep Form Creator config form.
 */
function multistep_form_creator_config_form($form, &$form_state) {
  $form = [];

  $form['message'] = [
    '#type'          => 'textfield',
    '#title'         => t('Mail message'),
    '#default_value' => variable_get('message', ''),
    '#required'      => TRUE,
  ];

  $form['title_separator'] = [
    '#type'          => 'textfield',
    '#title'         => t('Enter Separator'),
    '#default_value' => variable_get('title_separator', '-'),
    '#required'      => TRUE,
  ];

  $form['link_post_resource'] = [
    '#type'          => 'textfield',
    '#title'         => t('Link to post site'),
    '#default_value' => variable_get('link_post_resource', 'example@example.com'),
    '#required'      => TRUE,
  ];
  $form['admin_steps_amount'] = [
    '#type'          => 'select',
    '#title'         => t('Amount Steps.'),
    '#options'       => [
      1  => 1,
      2  => 2,
      3  => 3,
      4  => 4,
      5  => 5,
      6  => 6,
      7  => 7,
      8  => 8,
      9  => 9,
      10 => 10,
    ],
    '#default_value' => variable_get('admin_steps_amount', 3),
    '#description'   => t('Select amount of steps in form.'),
  ];

  $form['#submit'][] = 'multistep_form_creator_config_form_submit';

  return system_settings_form($form);
}

/**
 * Config form builder.
 *
 * @param array $form
 * @param array $form_state
 * @param int $step
 *
 * @return mixed
 */
function multistep_form_creator_config_step_form($form, &$form_state, $step = 1) {

  $form = [];
  $num = 's' . $step;

  $form[$step] = [
    '#type'        => 'fieldset',
    '#title'       => t('Step @num Form', ['@num' => $step]),
    '#description' => t('Enter field titles and descriptions.'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  ];

  $form[$step]['breadcrumb'] = [
    '#markup' => l(t('BACK TO STEPS'), 'admin/config/module-form-creator/steps/'),
    '#prefix' => '<div class="poll-form">',
    '#suffix' => '</div>',
  ];

  $form[$step][$num . '_expandable_step'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Expandable step'),
    '#default_value' => variable_get($num . '_expandable_step', 0),
    '#states'        => [
      'disabled' => [
        ':input[name="' . $num . '_active_step"]' => ['checked' => FALSE],
      ],
      'enabled'  => [
        ':input[name="' . $num . '_active_step"]' => ['checked' => TRUE],
      ],
    ],

  ];

  $form[$step][$num . '_active_step_title']       = [
    '#type'          => 'checkbox',
    '#title'         => t('Show step title'),
    '#return_value'  => 1,
    '#default_value' => variable_get($num . '_active_step_title', 0),
    '#states'        => [
      'disabled' => [
        ':input[name="' . $num . '_active_step"]' => ['checked' => FALSE],
      ],
      'enabled'  => [
        ':input[name="' . $num . '_active_step"]' => ['checked' => TRUE],
      ],
    ],
  ];
  $form[$step][$num . '_title']                   = [
    '#type'          => 'textfield',
    '#title'         => t('Title'),
    '#description'   => t('Title step.'),
    '#default_value' => variable_get($num . '_title', 'Title Step'),
    '#states'        => [
      'invisible' => [
        ':input[name="' . $num . '_active_step_title"]' => ['checked' => FALSE],
      ],
      'visible'   => [
        ':input[name="' . $num . '_active_step_title"]' => ['checked' => TRUE],
      ],
    ],
  ];
  $form[$step][$num . '_active_step_description'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Show step description'),
    '#default_value' => variable_get($num . '_active_step_description', 0),
  ];

  $form[$step][$num . '_description'] = [
    '#type'          => 'textfield',
    '#title'         => t('Description'),
    '#description'   => t('Description step.'),
    '#default_value' => variable_get($num . '_description', 'Description Step'),
    '#states'        => [
      'invisible' => [
        ':input[name="' . $num . '_active_step_description"]' => ['checked' => FALSE],
      ],
      'visible'   => [
        ':input[name="' . $num . '_active_step_description"]' => ['checked' => TRUE],
      ],
    ],
  ];

  for ($fs = 1; $fs <= 9; $fs++) {
    $fieldset               = get_fieldset_num($fs);
    $prefix                 = get_prefix($step, $fs);
    $form[$step][$fieldset] = [
      '#type'        => 'fieldset',
      '#title'       => t('Fieldset @num', ['@num' => $fs]),
      '#description' => t('Enter field title and descriptions for the FIELDSET.'),
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    ];
    // Fieldset.
    $form[$step][$fieldset][$prefix . '_active_title_fieldset'] = [
      '#type'          => 'checkbox',
      '#title'         => t('Show fieldset title'),
      '#default_value' => variable_get($prefix . '_active_title_fieldset', 0),
    ];
    $form[$step][$fieldset][$prefix . '_title_fieldset']        = [
      '#type'          => 'textfield',
      '#title'         => t('Step @num fieldset Title', ['@num' => $step]),
      '#default_value' => variable_get($prefix . '_title_fieldset', 'Title Fieldset'),
      '#required'      => TRUE,
      '#states'        => [
        'invisible' => [
          ':input[name="' . $prefix . '_active_title_fieldset"]' => ['checked' => FALSE],
        ],
        'visible'   => [
          ':input[name="' . $prefix . '_active_title_fieldset"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form[$step][$fieldset][$prefix . '_active_description_fieldset'] = [
      '#type'          => 'checkbox',
      '#title'         => t('Show fieldset description'),
      '#default_value' => variable_get($prefix . '_active_description_fieldset', 0),
    ];
    $form[$step][$fieldset][$prefix . '_description_fieldset']        = [
      '#type'          => 'textfield',
      '#title'         => t('Step @num fieldset Description', ['@num' => $step]),
      '#default_value' => variable_get($prefix . '_description_fieldset', 'Description Fieldset'),
      '#states'        => [
        'invisible' => [
          ':input[name="' . $prefix . '_active_description_fieldset"]' => ['checked' => FALSE],
        ],
        'visible'   => [
          ':input[name="' . $prefix . '_active_description_fieldset"]' => ['checked' => TRUE],
        ],
      ],
    ];

    for ($f = 1; $f <= 3; $f++) {
      $index = get_index($prefix, $f);
      // For always display first fieldset in active field config form.
      $form[$step][$fieldset][$index . '_active_field'] = [
        '#type'          => 'checkbox',
        '#title'         => t('Active field @num', ['@num' => $f]),
        '#default_value' => variable_get($index . '_active_field', 1),
        '#prefix'        => '<hr />',
      ];
      $form[$step][$fieldset][$index . '_required']     = [
        '#type'          => 'checkbox',
        '#title'         => t('Required field'),
        '#default_value' => variable_get($index . '_required', 0),
        '#states'        => [
          'disabled' => [
            ':input[name="' . $index . '_active_field"]' => ['checked' => FALSE],
          ],
          'enabled'  => [
            ':input[name="' . $index . '_active_field"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form[$step][$fieldset][$index . '_type_field']   = [
        '#type'          => 'select',
        '#title'         => t('Field @num Type', ['@num' => $f]),
        '#options'       => variable_get('types_fields'),
        '#default_value' => ($f !== 1) ? variable_get($index . '_type_field', '0') : variable_get($index . '_type_field', '1'),
        '#description'   => t('Select type of the field.'),
        '#states'        => [
          'disabled' => [
            ':input[name="' . $index . '_active_field"]' => ['checked' => FALSE],
          ],
          'enabled'  => [
            ':input[name="' . $index . '_active_field"]' => ['checked' => TRUE],
          ],
        ],
      ];

      $form[$step][$fieldset][$index . '_radios_choice_1'] = [
        '#type'          => 'textfield',
        '#title'         => t('Radios choice 1'),
        '#default_value' => variable_get($index . '_radios_choice_1'),
        '#description'   => t('Enter value For radios first choice.'),
        '#states'        => [
          'visible' => [
            ':input[name="' . $index . '_type_field"]' => ['value' => 1],
          ],
        ],
      ];

      $form[$step][$fieldset][$index . '_radios_choice_2'] = [
        '#type'          => 'textfield',
        '#title'         => t('Radios choice 2'),
        '#default_value' => variable_get($index . '_radios_choice_2'),
        '#description'   => t('Enter value For radios second choice.'),
        '#states'        => [
          'visible' => [
            ':input[name="' . $index . '_type_field"]' => ['value' => 1],
          ],
        ],
      ];

      $form[$step][$fieldset][$index . '_select_choices'] = [
        '#type'          => 'textarea',
        '#title'         => t('Select choices'),
        '#default_value' => variable_get($index . '_select_choices'),
        '#description'   => t('Enter values For select choices like that(key:value). Every couples from new line'),
        '#states'        => [
          'visible' => [
            ':input[name="' . $index . '_type_field"]' => ['value' => 7],
          ],
        ],
      ];

      $form[$step][$fieldset][$index . '_file_extensions'] = [
        '#type'          => 'textarea',
        '#title'         => t('Aviable File Extensions'),
        '#default_value' => variable_get($index . '_file_extensions'),
        '#description'   => t('Enter extensions For files like that(pdf). Every extension from new line'),
        '#states'        => [
          'visible' => [
            ':input[name="' . $index . '_type_field"]' => ['value' => 9],
          ],
        ],
      ];

      $form[$step][$fieldset][$index . '_show_title_field'] = [
        '#type'          => 'checkbox',
        '#title'         => t('Show title'),
        '#default_value' => variable_get($index . '_show_title_field', 1),
        '#states'        => [
          'disabled' => [
            ':input[name="' . $index . '_active_field"]' => ['checked' => FALSE],
          ],
          'enabled'  => [
            ':input[name="' . $index . '_active_field"]' => ['checked' => TRUE],
          ],
        ],
      ];

      $form[$step][$fieldset][$index . '_title_field'] = [
        '#type'          => 'textfield',
        '#title'         => t('Field @num Title', ['@num' => $f]),
        '#default_value' => variable_get($index . '_title_field', 'Title Field'),
        '#required'      => TRUE,
        '#states'        => [
          'invisible' => [
            ':input[name="' . $index . '_show_title_field"]' => ['checked' => FALSE],
          ],
          'visible'   => [
            ':input[name="' . $index . '_show_title_field"]' => ['checked' => TRUE],
          ],
          'disabled'  => [
            ':input[name="' . $index . '_active_field"]' => ['checked' => FALSE],
          ],
          'enabled'   => [
            ':input[name="' . $index . '_active_field"]' => ['checked' => TRUE],
          ],

        ],
      ];

      $form[$step][$fieldset][$index . '_show_description_field'] = [
        '#type'          => 'checkbox',
        '#title'         => t('Show description'),
        '#default_value' => variable_get($index . '_show_description_field', 0),
        '#states'        => [
          'disabled' => [
            ':input[name="' . $index . '_active_field"]' => ['checked' => FALSE],
          ],
          'enabled'  => [
            ':input[name="' . $index . '_active_field"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form[$step][$fieldset][$index . '_description_field']      = [
        '#type'          => 'textfield',
        '#title'         => t('Field @num Description', ['@num' => $f]),
        '#default_value' => variable_get($index . '_description_field', 'Description Field'),
        '#states'        => [
          'invisible' => [
            ':input[name="' . $index . '_show_description_field"]' => ['checked' => FALSE],
          ],
          'visible'   => [
            ':input[name="' . $index . '_show_description_field"]' => ['checked' => TRUE],
          ],
          'disabled'  => [
            ':input[name="' . $index . '_active_field"]' => ['checked' => FALSE],
          ],
          'enabled'   => [
            ':input[name="' . $index . '_active_field"]' => ['checked' => TRUE],
          ],

        ],
      ];
    }
  }

  $form['#submit'][] = 'multistep_form_creator_config_form_submit';

  return system_settings_form($form);
}
