<?php

/**
 * @file
 */

/**
 * Admin page callback.
 *
 * @return string
 * @throws \Exception
 */
function steps_list() {

  $sum_steps = variable_get('admin_steps_amount', 3);

  $header = [
    ['data' => t('Step'), 'field' => 'title', 'sort' => 'ASC'],
  ];

  $row = [];

  if ($sum_steps) {

    for ($s = 1; $s <= $sum_steps; $s++) {
      $step  = l(t('Step-@step', ['@step' => $s]), 'admin/config/module-form-creator/steps/' . $s);
      $row[] = [
        ['data' => $step],
      ];
    }
  }

  $output = theme('table', [
    'header' => $header,
    'rows'   => $row,
  ]);
  $output .= theme('pager');
  return $output;
}
