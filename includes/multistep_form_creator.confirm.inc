<?php

/**
 * @file
 * Handles the form confirmation of the multistep form creator form.
 */

/**
 * Confirm data for the confirm step.
 */
function multistep_form_creator_confirm_details_form($form, &$form_state, $step) {
  $values    = isset($form_state['multistep_values']) ? $form_state['multistep_values'] : [];
  $separator = variable_get('title_separator');

  // Start step.
  for ($fs = 1; $fs <= 6; $fs++) {
    $prefix          = get_prefix($step, $fs);
    $length_fieldset = variable_get($prefix . '_f') ? variable_get($prefix . '_f') : 0;

    if (empty($length_fieldset)) {
      continue;
    }

    // Start field.
    for ($f = 1; $f <= $length_fieldset; $f++) {

      $index = get_index($prefix, $f);

      // If field is active.
      if (NULL !== variable_get($index . '_active_field') || 1 === $f) {
        $data       = isset($values[$step][$index . '_title_field']) ? $values[$step][$index . '_title_field'] : 0;
        $type_field = variable_get($index . '_type_field');
        $data       = get_confirm_data($type_field, $data);

        $details[] = [
          'field'     => variable_get($index . '_title_field', 'Title'),
          'separator' => $separator,
          'data'      => $data,
        ];
      }
    }
  }
  return theme('confirm_details', ['details' => $details]);
}

/**
 * Confirm data for the confirm expandable step.
 */
function multistep_form_creator_confirm_details_expandable_form($form, $form_state, $step) {
  $values    = isset($form_state['multistep_values'][$step][$step]) ? $form_state['multistep_values'][$step][$step] : [];
  $separator = variable_get('title_separator');
  $steps     = variable_get('admin_steps_amount', 3);

  // Start step.
  foreach ($values as $key => $value) {
    $detail = [];

    for ($fs = 1; $fs <= $steps; $fs++) {
      $prefix          = get_prefix($step, $fs);
      $fieldset_num    = get_fieldset_num($fs);
      $length_fieldset = variable_get($prefix . '_f') ? variable_get($prefix . '_f') : 0;

      if (empty($length_fieldset)) {
        continue;
      }

      // Start field.
      for ($f = 1; $f <= $length_fieldset; $f++) {

        $index = get_index($prefix, $f);

        // If field is active.
        if (NULL !== variable_get($index . '_active_field') || 1 === $f) {
          $data       = isset($values[$key][$fieldset_num][$index . '_title_field']) ? $values[$key][$fieldset_num][$index . '_title_field'] : 0;
          $type_field = variable_get($index . '_type_field');
          $data       = get_confirm_data($type_field, $data);

          $detail[] = [
            'field'     => variable_get($index . '_title_field', 'Title'),
            'separator' => $separator,
            'data'      => $data,
          ];
        }
      }
    }
    $details[] = [
      'title'  => t('Sub form'),
      'data'   => $detail,
      'number' => $key + 1,
    ];
  }
  return theme('confirm_details_expandable', ['details' => $details]);
}
