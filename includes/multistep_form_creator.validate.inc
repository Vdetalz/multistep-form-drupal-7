<?php

/**
 * @file
 * Handles the form validation of the multistep form creator form.
 *
 * All hooks are in the .module file.
 */

/**
 * Master validation function for the multistep form creator form.
 *
 * Uses per-stage validation and calls functions for each one.
 */
function multistep_form_creator_form_validate($form, &$form_state) {

  if ('Back' == $form_state['triggering_element']['#value']) {
    return;
  }
  $steps        = variable_get('admin_steps_amount', 3);
  $current_step = $form_state['stage'];

  $confirm = intval($steps) + 1;
  $finish  = intval($steps) + 2;

  switch ($form_state['stage']) {

    case $confirm:
      multistep_form_creator_confirm_validate($form, $form_state);
      break;

    default:
      multistep_form_creator_step_validate($form, $form_state);
  }
}

/**
 * Validation for the winery_details step.
 */
function multistep_form_creator_step_validate($form, &$form_state) {
  if (TRUE === $form_state['has_file_element']) {
    mfc_file_validate($form_state);
  }
}

/**
 * Validate file field.
 *
 * @param $form_state
 */
function mfc_file_validate(&$form_state) {
  foreach ($form_state['values'] as $value => $name) {

    if (!mfc_is_file_field($value)) {
      continue;
    }
    $settings =  [
      //'file_validate_is_image'   => [],
      'file_validate_extensions' => get_file_extensions($value),
    ];

    $file = file_save_upload($value, $settings);

    if ($file) {
      // Move the file into the Drupal file system.
      if ($file = file_move($file, 'public://')) {
        // Save the file for use in the submit handler.
        $form_state['storage'][$value] = $file;
      }
      else {
        form_set_error('file', t("Failed to write the uploaded file to the site's file folder."));
      }
    }
    else {
      form_set_error('file', t('No file was uploaded.'));
    }
  }
}

/**
 * Check type field.
 *
 * @param string $name
 *
 * @return bool
 *   Returns TRUE if the file and FALSE otherwise.
 */
function mfc_is_file_field($name = '') {
  $identity = mb_substr($name, strrpos($name, '_'), NULL, 'utf-8');
  if ('_file' === $identity) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Validation for the confirm step.
 */
function multistep_form_creator_confirm_validate($form, &$form_state) {
  if (!valid_email_address($form_state['values']['confirm']['term']['email']) && 'Next' == $form_state['triggering_element']['#value']) {
    form_set_error('confirm', "You must enter correct email before continue");
  }
}
