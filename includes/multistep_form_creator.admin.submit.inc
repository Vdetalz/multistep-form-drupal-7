<?php

/**
 * @file
 * Form submission of the multistep form creator module.
 */

/**
 * MultiStep Form Creator submit form.
 */
function multistep_form_creator_config_form_submit($form, &$form_state) {

  $current_step = isset($form_state['build_info']['args'][0]) ? (int) $form_state['build_info']['args'][0] : 1;

  $sum_steps = variable_get('admin_steps_amount', 3);

  // Amount fields in the current fieldset.
  $count_fields = 0;
  // Steps.
  for ($s = 1; $s <= $sum_steps; $s++) {
    $num = 's' . $s;
    if ($s !== $current_step) {
      continue;
    }
    // Fieldsets.
    for ($fs = 1; $fs <= 9; $fs++) {
      $prefix = get_prefix($s, $fs);
      // Fields.
      for ($f = 1; $f <= 3; $f++) {
        $index = get_index($prefix, $f);

        if (isset($form_state['values'][$index . '_active_field']) && '1' == $form_state['values'][$index . '_active_field']) {
          $count_fields++;
          if (isset($form_state['values'][$index . '_show_description_field']) && '1' == $form_state['values'][$index . '_show_description_field']) {
            variable_set($index . '_show_description_field', TRUE);
          }
          else {
            variable_set($index . '_show_description_field', FALSE);
          }

          if (isset($form_state['values'][$index . '_show_title_field']) && '1' == $form_state['values'][$index . '_show_title_field']) {
            variable_set($index . '_show_title_field', TRUE);
          }
          else {
            variable_set($index . '_show_title_field', FALSE);
          }

          if (isset($form_state['values'][$index . '_file_extensions'])) {
            variable_set($index . '_file_extensions', $form_state['values'][$index . '_file_extensions']);
          }
        }
        // Required or not.
        if (isset($form_state['values'][$index . '_required']) && '1' == $form_state['values'][$index . '_required']) {
          variable_set($index . '_required', TRUE);
        }
        else {
          variable_set($index . '_required', FALSE);
        }
      }
      // Amount fields in the current fieldset.
      variable_set($prefix . '_f', $count_fields);
      $count_fields = 0;
    }

    // Expandable step or no.
    if (isset($form_state['values'][$num . '_expandable_step'])) {
      variable_set($num . '_expandable_step', $form_state['values'][$num . '_expandable_step']);
    }

  }

  if (isset($form_state['admin_steps_amount'])) {
    variable_set('admin_steps_amount', $form_state['admin_steps_amount']);
  }
  drupal_flush_all_caches();
}
