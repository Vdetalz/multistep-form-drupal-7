<?php

/**
 * @file
 * Displays the confirm winery details.
 *
 * Available variables:
 * - $win_details: An array of the form_state to display.
 */
?>
<table>
  <?php foreach ($details as $detail): ?>
    <tr>
      <td><strong><?php echo $detail['field'];
          echo $detail['separator']; ?></strong></td>
      <td><?php echo $detail['data']; ?></td>
    </tr>
  <?php endforeach; ?>
</table>
<hr/>
