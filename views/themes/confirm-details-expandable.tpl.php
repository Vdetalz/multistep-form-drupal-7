<?php

/**
 * @file
 * Displays the confirm winery details.
 *
 * Available variables:
 * - $details: An array of the form_state to display.
 * - $details contains of:
 *  - title,
 *  - number,
 *  - data(array):
 *    -data,
 *    -separator,
 *    -field.
 */
?>
<?php for ($step = 0; $step < count($details); $step++): ?>
  <table>
    <p><?php echo $details[$step]['number'] . ' - ' . $details[$step]['title'] ?></p>
    <?php for ($fs = 0; $fs < count($details[$step]['data']); $fs++): ?>
      <tr>
        <td><strong><?php echo $details[$step]['data'][$fs]['field'];
            echo $details[$step]['data'][$fs]['separator']; ?></strong></td>
        <td><?php echo $details[$step]['data'][$fs]['data']; ?></td>
      </tr>
    <?php endfor; ?>
  </table>
<?php endfor; ?>
<hr/>
